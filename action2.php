<?php
    session_start();
    ob_start();
    function base(){
        return str_replace("action2.php","", $_SERVER['PHP_SELF']);
    }
    if ($_SESSION['key'] < $_SESSION['how_many']) {
        $setQ = "pytanie".($_SESSION['key'] + 1);
    }  else {
        $setQ = 'dzieki';
    }
    if (!empty($_POST['answer'])) {
        switch ($_SESSION['questionType'][0])
        {
            case 'normal':
                $_SESSION['answersArray'][$_SESSION['key']-1] = $_POST['answer'];
                break;
            case 'yesNo':
                if ($_POST['answer'] == "Nie") {
                    $setQ = "pytanie".($_SESSION['key'] + 1 + $_SESSION['questionType'][1]);
                }
                $_SESSION['answersArray'][$_SESSION['key']-1] = $_POST['answer'];
                break;
            case 'specialRadio':
                $_SESSION['answersArray'][$_SESSION['key']-1] = $_POST['answer'];
                break;
            case 'multiChoice':
                foreach ($_POST['answer'] as $answerKey => $answerValue) {
                    if ($answerKey == 0)
                    {
                       switch ($answerValue) {
                        case 'Inny':
                            $choices = htmlentities($_POST['answer2'],ENT_QUOTES, "UTF-8");
                            break;
                        default:
                            $choices = $answerValue;
                            break;
                        }
                    }
                    else
                    {
                        switch ($answerValue) {
                        case 'Inny':
                            $choices = $choices.'\r\n'.htmlentities($_POST['answer2'],ENT_QUOTES, "UTF-8");
                            break;
                        default:
                            $choices = $choices.'\r\n'.$answerValue;
                            break;
                        }
                    }
                }
                $_SESSION['answersArray'][$_SESSION['key']-1] = $choices;
                break;
            case 'text':
                $text = htmlentities($_POST['answer'],ENT_QUOTES, "UTF-8");
                $_SESSION['answersArray'][$_SESSION['key']-1] = $text;
                break;
            default:
                break;
        }
        $_SESSION['custom_error'] = null;
        $locationString = base().'ankieta--'.$setQ;
    } else {
        switch ($_SESSION['questionType'][0])
        {
            case 'normal':
                $_SESSION['custom_error'] = "Prosze zaznaczyc odpowiedź.";
                break;
            case 'yesNo':
                $_SESSION['custom_error'] = "Prosze zaznaczyc odpowiedź.";
                break;
            case 'specialRadio':
                $_SESSION['custom_error'] = "Prosze zaznaczyc odpowiedź.";
                break;
            case 'multiChoice':
                $_SESSION['custom_error'] = "Prosze zaznaczyc odpowiedzi.";
                break;
            case 'text':
                $_SESSION['custom_error'] = "Prosze wpisać odpowiedź.";
                break;
            default:
                $_SESSION['custom_error'] = "Prosze zaznaczyc odpowiedź.";
                break;
        }
        $locationString = base().'ankieta--pytanie'.$_SESSION['key'];
    }
    if (isset($locationString)) {
        header('Location: '.$locationString);
    } else {
        echo "problem z przejsciem";
    }
?>