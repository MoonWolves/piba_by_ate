<?php
    if ($_SESSION['key'] < $_SESSION['how_many']) {
        $inputValue = 'Następne pytanie';
    }  else {
        $inputValue = 'Wyślij odpowiedzi';
    }

    $possible_answer = explode("\r\n",$element['possible_answers']);
    $questionType = explode("-",$element['question_type']);
    $_SESSION['questionType'] = $questionType;
    switch ($questionType[0]) {
        case 'normal':
            for ($i=0; $i < sizeof($possible_answer); $i++) :?>
            <h3>
                <input type="radio" name="answer" value="<?php echo $possible_answer[$i]?>"><?php echo $possible_answer[$i]?>
            </h3>
            <?php endfor;
            break;
        case 'yesNo': ?>
            <h3>
                <?php for ($i=0; $i < sizeof($possible_answer); $i++) :?>
                    <span style="display:inline-block">
                        <input type="radio" name="answer" value="<?php echo $possible_answer[$i]?>"/>
                        <?php echo $possible_answer[$i]?>
                    </span>
                <?php endfor; ?>
            </h3>
            <?php
            break;
        case 'specialRadio': ?>
            <h3>
                <label for="answer_label" >Żle - </label>
                <?php for ($i=0; $i < sizeof($possible_answer); $i++) :?>
                    <span name="answer_label" style="display:inline-block">
                        <label for="answer" style="display:block"><?php echo $possible_answer[$i]?></label>
                        <input type="radio" name="answer" value="<?php echo $possible_answer[$i]?>"/>
                    </span>
                <?php endfor; ?>
                <label for="answer_label" > - Bardzo dobrze</label>
            </h3>
            <?php
            break;
        case 'multiChoice':
            for ($i=0; $i < sizeof($possible_answer); $i++) :
                if ($possible_answer[$i] == 'Inny') :?>
                <h3>
                    <input type="checkbox" name="answer[]" value="<?php echo $possible_answer[$i]?>"><?php echo $possible_answer[$i]?>
                    <input type="text" name="answer2" placeholder="(podaj jaki)" >
                <?php else:?>
                    <input type="checkbox" name="answer[]" value="<?php echo $possible_answer[$i]?>"><?php echo $possible_answer[$i]?>
                <?php endif?>
                </h3>
            <?php endfor;
            break;
        case 'text': ?>
            <h3>
                <label for="answer">Wpisz nazwe: </label>
                <input type="text" name="answer" placeholder="nazwa">
            </h3>
            <?php
            break;
        default:
            echo 'Nieznany typ pytania';
            break;
    }
    if (!is_null($_SESSION['custom_error'])) {
        echo $_SESSION['custom_error'];
    }
?>
    <br>
    <input type='submit' value='<?php echo $inputValue;?>'>
