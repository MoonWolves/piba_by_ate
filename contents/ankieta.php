<?php
    include "mysql/get_questions.php";
    if (!isset($_SESSION['answersArray'])) {
        $_SESSION['answersArray'] = array_fill(0, $how_many, NULL);
    }
?>

<div id="container_ankieta">
    <div class="square_ankieta">
        <div class="tile_ankieta">
            <ul>
            <?php
            for ($i=0; $i <= $how_many; $i++) :
                if ($i > 0 && $i <= $how_many) {
                    $text = "Pytanie ".($i);
                    $value = "pytanie".$i;
                } elseif  ($i == 0) {
                    $text = "Cześć";
                    $value = "opowiedz-o-sobie";
                } else {
                    $text = "Koniec ";
                    $value = "dzieki";
                }
                ?>
                <a href="<?php echo base()."ankieta--".$value; ?>">
                    <li class="ank-btn"><?php echo $text; ?></li>
                </a>
            <?php endfor;?>
            </ul>
        </div>
        <div style="clear:both"></div>
    </div>
    <div class="square_ankieta1">
        <div class="tile_ankieta1">
            <?php if(isset($url[1])) : ?>
                <?php if ($url[1] == "opowiedz-o-sobie") :?>
                    <form action='action1.php' method="post">
                        <h1>Cześć!</h1>
                        <h2>Zanim wypełnisz ankietę, prosimy Cię o podanie tych danych.</h2>
                        <label for="email_adress">E - mail</label>
                        <input type="email" name="email_adress" value="" placeholder="przykład@domena.pl">
                        <br><br>
                        <label for="gender" >Płeć</label>
                        <span name="gender" style="display:inline-block">
                            <label for="gender" style="display:block">Kobieta</label>
                            <input type="radio" name="gender" value="Kobieta"/>
                        </span>
                        <span style="display:inline-block">
                            <label for="gender" style="display:block">Mężczyzna</label></label>
                            <input type="radio" name="gender" value="Mężczyzna"/>
                        </span>
                        <span style="display:inline-block">
                            <label for="gender" style="display:block">Inna</label></label>
                            <input type="radio" name="gender" value="Inna"/>
                        </span>
                        <br><br>
                        <label for="birth_date">Data urodzenia</label>
                        <input type="date" name="birth_date"><br>
                        <?php  if (!is_null($_SESSION['custom_error'])) echo $_SESSION['custom_error'];?>
                        <br>
                        <input type='submit' value='Przejdź do ankiety'>
                    </form>
                <?php elseif ($url[1] == "dzieki") :?>
                    <h1>Dzięki, za wypełnienie ankiety</h1>
                    <?php include 'mysql/push_answers.php';?>
                <?php else :?>
                    <?php
                        $question_url_number = explode('e',$url[1]);
                        $question_url_number = $question_url_number[1] - 1;
                    ?>
                    <?php foreach($array as $key => $element) : ?>
                        <?php if($key == $question_url_number) : ?>
                        <?php $_SESSION['key'] = $key + 1?>
                            <form enctype="multipart/form-data" action="action2.php" method="post">
                                <h1><?php echo $element['id'] ?>. <?php echo $element['question'] ?></h1>
                                <?php include 'contents/question_types.php';?>
                            </form>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif;?>
            <?php endif; ?>
        </div>
    </div>
    <div style="clear:both"></div>
</div>
