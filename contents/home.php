 <div class="square">
     <div class="tile1">
         <a href="<?php base();?>raport" class="tilelink"><i class="icon-user"></i> <br />
             Raport </a>
     </div>
     <div style="clear:both"></div>
     <div class="tile3">
         <a href="<?php base();?>jaki-cel-ankiety" class="tilelink1"><i class="icon-help-circled"></i><br />
             Cel ankiety </a>
     </div>
     <div style="clear:both"></div>
     <a href="https://pl.wikipedia.org/wiki/Stephen_King" class="tilelink2" target="_blank"
         title="Przeczytaj o Stephen Kingu">
         <div class="tile4">
             <i>Nawet z pozoru najdziwaczniejsza, najskromniejsza pasja jest czymś bardzo, ale to bardzo cennym.</i>
             <br />
             <i>Stephen King</i>
         </div>
     </a>
     <div style="clear:both"></div>
 </div>
 <div class="square">
     <a href="<?php base();?>ankieta--opowiedz-o-sobie" class="tilelink3">
         <div class="tile5">
             <i class="icon-laptop"></i><br />
             Uzupełnij Naszą ankietę!
         </div>
     </a>
     <div class="fb">
         <a href="https://www.facebook.com/asia.zawadzka.5815" class="tilelinkfbad" target="_blank"><i
                 class="icon-facebook-squared" title="Zobacz profil Joanny"></i></a>
     </div>
     <div class="fb">
         <a href="https://www.facebook.com/addig5" class="tilelinkfbad" target="_blank"><i class="icon-facebook-squared"
                 title="Zobacz profil Adriana"></i></a>
     </div>
     <div class="fb">
         <a href="https://www.facebook.com/adam.rozmus.90" class="tilelinkfbad" target="_blank"><i class="icon-facebook-squared"
                 title="Zobacz profil Adama"></i></a>
     </div>
 </div>
 <div style="clear:both"></div>