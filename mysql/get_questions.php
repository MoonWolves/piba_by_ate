<?php
require_once "mysql/connection_cfg.php";
$connection = @new mysqli($host, $db_username, $db_password, $db_name);

if ($connection->connect_errno!=0)
{
    echo "Error: ".$connection->connect_errno;
}
else
{
    mysqli_query($connection, "SET CHARSET utf8");
    mysqli_query($connection, "SET NAMES 'utf8' COLLATE 'utf8_polish_ci'");
    $query = "SELECT * FROM  questions";
    $result = mysqli_query($connection, $query);
    $how_many = mysqli_num_rows($result);
    $_SESSION['how_many'] = $how_many;
    if ($how_many>=1)
    {
        $array = array();
        for ($i=0; $i < $how_many ; $i++)
        {
            $row = mysqli_fetch_assoc($result);
            $array[] = $row;
        }
    }
    $connection->close();
}
?>