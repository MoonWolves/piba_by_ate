-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 03 Cze 2019, 00:11
-- Wersja serwera: 10.1.39-MariaDB
-- Wersja PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `piba`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `answer0` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer1` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer2` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer3` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer4` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer5` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer6` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer7` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer8` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer9` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer10` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer11` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer12` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer13` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer14` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer15` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `answer16` varchar(200) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `possible_answers` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `question_type` varchar(50) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `questions`
--

INSERT INTO `questions` (`id`, `question`, `possible_answers`, `question_type`) VALUES
(1, 'Oceń swoje zdolności muzyczne.', '1\r\n2\r\n3\r\n4\r\n5', 'specialRadio'),
(2, 'Zajmujesz się muzyką?', 'Tak\r\nNie', 'yesNo-7'),
(3, 'Grasz na instrumencie?', 'Tak\r\nNie', 'yesNo-1'),
(4, 'Na jakim instrumencie grasz?', 'Gitara\r\nPianino\r\nFortepian\r\nSkrzypce\r\nFlet\r\nInny', 'multiChoice'),
(5, 'Śpiewasz?', 'Tak\r\nNie', 'yesNo-0'),
(6, 'Jak często ćwiczysz swoje muzyczne umiejętności?', 'Bardzo często (więcej niż 6 razy w tygodniu)\r\nCzęsto ( 3-6 razy w tygodniu)\r\nRzadko (1-4) razy w tygodniu\r\nNigdy', 'normal'),
(7, 'Ile lat zajmujesz się muzyką?', 'Więcej niż 15 lat\r\nMiędzy 10 a 15 lat\r\nMiędzy 5 a 10 lat\r\nMniej niż 5 lat', 'normal'),
(8, 'Należysz do jakiegoś zespołu?', 'Tak\r\nNie', 'yesNo-1'),
(9, 'Jak się nazywa?', '', 'text'),
(10, 'Oceń swoje zdolności sportowe', '1\r\n2\r\n3\r\n4\r\n5', 'specialRadio'),
(11, 'Uprawiasz jakiś sport?', 'Tak\r\nNie', 'yesNo-5'),
(12, 'Jaki? ', 'Piłka nożna\r\nSiatkówka \r\nKoszykówka\r\nSztuki walki\r\nPiłka ręczna\r\nTenis\r\nTaniec\r\nPływanie\r\nBieganie\r\nJazda na rowerze\r\nJoga\r\nFitness\r\nInny', 'multiChoice'),
(13, 'Jak często trenujesz?', 'Bardzo często (więcej niż 5 razy w tygodniu po 2 godziny)\r\nCzęsto -( 3-6 razy w tygodniu po 2 godziny)\r\nRzadko (1-4) razy w tygodniu po 2 h\r\nNigdy', 'normal'),
(14, 'Ile lat trenujesz?', 'Więcej niż 15 lat\r\nMiędzy 10 a 15 lat\r\nMiędzy 5 a 10 lat\r\nMniej niż 5 lat', 'normal'),
(15, 'Należysz jakiegoś klubu sportowego?', 'Tak\r\nNie', 'yesNo-1'),
(16, 'Jak się nazywa?', '', 'text'),
(17, 'Co wolisz?', 'Muzyka\r\nSport\r\nUwielbiam oba\r\nŻadne z powyższych', 'normal');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email_adress` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_polish_ci NOT NULL,
  `birth_date` date NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indeksy dla tabeli `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
