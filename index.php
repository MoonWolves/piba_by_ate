<?php
    session_start();
    ob_start();
    error_reporting(0);
    $url = explode('--',$_SERVER['QUERY_STRING']);
    function base()
    {
        return str_replace("index.php","", $_SERVER['PHP_SELF']);
    }
?>
<!DOCTYPE HTML>
<html lang="pl">
<?php include_once 'parts/header.php';?>

<body onload="odliczanie();">
    <div id="container">
        <?php include_once 'parts/nav.php';?>
        <div id="content">
            <?php
            switch ($url[0])
            {
                case 'ankieta-hobby-pasje':
                    include 'contents/home.php';
                    break;
                case 'ankieta':
                    include 'contents/ankieta.php';
                    break;
                case 'raport':
                    header('Location: raport.php');
                    break;
                case 'jaki-cel-ankiety':
                    include 'contents/celankiety.php';
                    break;
                default:
                    include 'contents/home.php';
                    break;
            }
            ?>
        </div>
        <?php include_once 'parts/footer.php';?>
    </div>
</body>

</html>