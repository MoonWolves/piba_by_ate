<?php
    session_start();
    ob_start();
    require_once('mysql/is_in_database.php');
    function base()
    {
        return str_replace("action1.php","", $_SERVER['PHP_SELF']);
    }
    if (isset($_POST['email_adress']) && isset($_POST['gender']) && isset($_POST['birth_date']))
    {
        $_SESSION['gender'] = $_POST['gender'];
        $_SESSION['birth_date'] = $_POST['birth_date'];
        if (is_in_db('email_adress', $_POST['email_adress'])) {
            $_SESSION['custom_error'] = "Adres email jest już w bazie. <br> Prosze podać inny.";
            $locationString = base().'ankieta--opowiedz-o-sobie';
        }
        else
        {
            $_SESSION['custom_error'] = null;
            $_SESSION['email_adress'] = $_POST['email_adress'];
            $locationString = base().'ankieta--pytanie1';
        }
    }
    else
    {
        $_SESSION['custom_error'] = "Prosze zaznaczyc wszystkie odpowiedzi. ";
        $locationString = base().'ankieta--opowiedz-o-sobie';
    }
    if (isset($locationString)) {
        header('Location: '.$locationString);
    }
    else {
        echo "problem z przejsciem";
    }
?>