<div id="footer" class="rectangle">
            2019 &copy; Joanna Zawadzka i Adran Widuch oraz Adam Rozmus zapraszają do wypełnienia ankiety. <br>
            <a href="mailto:joanna.zawadzka@edu.uekat.pl"><i class="icon-mail-alt">joanna.zawadzka@edu.uekat.pl</i></a>
            <a href="mailto:adrian.widuch@edu.uekat.pl"><i class="icon-mail-alt">adrian.widuch@edu.uekat.pl</i></a>
            <a href="mailto:adam.rozmus@edu.ueakat.pl"><i class="icon-mail-alt">adam.rozmus@edu.ueakat.pl</i></a>
</div>